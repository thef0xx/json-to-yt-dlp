import json
import subprocess

# Prompt for the JSON file name
filename = input("Enter the JSON file name: ")

# Prompt for audio conversion choice
convert_audio = input("Convert videos to WAV audio files? (Y/N): ").lower() == "y"

# Load the JSON file
with open(filename) as f:
    for line in f:
        try:
            data = json.loads(line)
            # Extract video URL from the JSON data
            url = data['webpage_url']
            # Download the video using yt-dlp
            if convert_audio:
                subprocess.run(['yt-dlp', url, '-x', '--audio-format', 'wav'])
            else:
                subprocess.run(['yt-dlp', url])
        except json.JSONDecodeError:
            continue

