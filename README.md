# json to yt-dlp

This script allows you to extract a YouTube playlist to a JSON file and download the videos from the playlist using `yt-dlp`.

## Prerequisites

Before using this script, ensure that you have the following installed on your system:
- Python (version 3 or above)
- yt-dlp
- ffmpeg and ffprobe (Required for merging separate video and audio files as well as for various post-processing tasks)

## Extracting Playlist to JSON

To extract a YouTube playlist to a JSON file, follow these steps:

1. Install `yt-dlp` by running the following command: `pip install yt-dlp`

2. Open a terminal or command prompt and navigate to the directory where you want to save the JSON file.

3. Run the following command, replacing `<playlist link>` with the URL of the YouTube playlist you want to extract: `yt-dlp --flatplaylist --dump-json <playlist link> > playlist.json`

This command uses `yt-dlp` to extract the playlist data in JSON format and save it to a file named `playlist.json`.

## Downloading Videos from JSON

To download the videos from the JSON file using the provided Python script, follow these steps:

1. Save the Python script `script.py` to the same directory where you saved the JSON file.

2. Open a terminal or command prompt and navigate to the directory where the Python script is saved.

3. Run the following command: `python3 script.py`

This command will read the JSON file, extract the video URLs, and download each video using `yt-dlp`.

> Note: Make sure you have `yt-dlp` installed on your system for the script to work.